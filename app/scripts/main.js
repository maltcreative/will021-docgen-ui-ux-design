console.log('\'Allo \'Allo!');


$(document).ready(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});


$(document).ready(function() {
	$('.first-item').on('click', function() {
		$('.form-01').fadeToggle();
		$('.form-02, .form-03').hide();
		$('.second-item, .third-item').removeClass('active');
		$(this).toggleClass('active');
	});
	$('.second-item').on('click', function() {
		$('.form-02').fadeToggle();
		$('.form-01, .form-03, .other-documents').hide();
		$('.first-item, .third-item').removeClass('active');
		$(this).toggleClass('active');
	});
	$('.third-item').on('click', function() {
		$('.form-03').fadeToggle();
		$('.form-01, .form-02').hide();
		$('.first-item, .second-item').removeClass('active');
		$(this).toggleClass('active');
	});
});

$(document).ready(function() {
	$('.show-it').on('click', function() {
		$('.single-doc-popup').fadeIn();
		$('.close-it').on('click', function(){
			$('.single-doc-popup').fadeOut();
			$('.form-01 tr, .form-02 tr, .form-03 tr').removeClass('active-row');
		})
	})
});

$(document).ready(function() {
    $('.checkbox.to-email input[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
            $('.enter-email').slideDown();
        }
        else if($(this).is(":not(:checked)")){
            $('.enter-email').slideUp();
        }
    });
});

$(document).ready(function() {
	$('select.letters-correspondence').change(function() {
	     $(this).children(':selected').attr('selected', true);
	     $('.new-document.letters-correspondence').fadeIn();
		 $('.new-document.slips-certs').hide();
	     $('.new-document.reports-and-manuals').hide();
		 $('.edit-slips-certs-wording').hide();
	     $('.new-document.other-documents').hide();
		 $("select.slips-certificates-wording").val($("#target option:first").val());
		 $("select.reports-manuals").val($("#target option:first").val());
		 $("select.other-docs").val($("#target option:first").val());
	    $('html, body').animate({
	      scrollTop: $("#third-scroll").offset().top
	    }, 1400);
	});
});


$(document).ready(function() {
	$('select.slips-certificates-wording').change(function() {
	     $(this).children(':selected').attr('selected', true);
	     $('.new-document.slips-certs').fadeIn();
		 $('.new-document.letters-correspondence').hide();
	     $('.new-document.reports-and-manuals').hide();
	     $('.new-document.other-documents').hide();
		 $("select.letters-correspondence").val($("#target option:first").val());
		 $("select.reports-manuals").val($("#target option:first").val());
		 $("select.other-docs").val($("#target option:first").val());
	    $('html, body').animate({
	      scrollTop: $("#third-scroll").offset().top
	    }, 1400);
	});
});

$(document).ready(function() {
	$('select.reports-manuals').change(function() {
	     $(this).children(':selected').attr('selected', true);
	     $('.new-document.reports-and-manuals').fadeIn();
	     $('.new-document.slips-certs').hide();
		 $('.new-document.letters-correspondence').hide();
		 $('.edit-slips-certs-wording').hide();
	     $('.new-document.other-documents').hide();
		 $("select.letters-correspondence").val($("#target option:first").val());
		 $("select.slips-certificates-wording").val($("#target option:first").val());
		 $("select.other-docs").val($("#target option:first").val());
	    $('html, body').animate({
	      scrollTop: $("#third-scroll").offset().top
	    }, 1400);
	});
});


$(document).ready(function() {
	$('select.other-docs').change(function() {
	     $(this).children(':selected').attr('selected', true);
	     $('.new-document.other-documents').fadeIn();
	     $('.new-document.reports-and-manuals').hide();
	     $('.new-document.slips-certs').hide();
		 $('.new-document.letters-correspondence').hide();
		 $('.edit-slips-certs-wording').hide();
		 $("select.letters-correspondence").val($("#target option:first").val());
		 $("select.reports-manuals").val($("#target option:first").val());
		 $("select.slips-certificates-wording").val($("#target option:first").val());
	    $('html, body').animate({
	      scrollTop: $("#third-scroll").offset().top
	    }, 1400);
	});
});
















$(document).ready(function() {
	$('.amending-current-document span.editable').click(function() {
	  var boxes = $('.edit-document');
	  var filter = $(this).data("filter");
	  boxes.hide();
	  $('tr').removeClass('active-row')
	  $(this).parents('tr').addClass('active-row');
	  if (filter == "all") {
		boxes.fadeIn();
	  } else {
		filter = '.' + filter;
		boxes.filter(filter).fadeIn();
	  }
	    $('html, body').animate({
	      scrollTop: $("#second-scroll").offset().top
	    }, 1400);
	  return false;
	});
});


$(document).ready(function() {
	$('.archive-docs span.editable').click(function() {
	  var boxes = $('.edit-document');
	  var filter = $(this).data("filter");
	  boxes.hide();
	  $('tr').removeClass('active-row')
	  $(this).parents('tr').addClass('active-row');
	  if (filter == "all") {
		boxes.fadeIn();
	  } else {
		filter = '.' + filter;
		boxes.filter(filter).fadeIn();
	  }
	  return false;
	});
});


$(document).ready(function() {
	$('span.editable.show-it-policy').click(function() {
	  var boxes = $('.edit-slips-certs-wording');
	  var filter = $(this).data("filter");
	  boxes.hide();
	  $('tr').removeClass('active-row')
	  $(this).parents('tr').addClass('active-row');
	  if (filter == "all") {
		boxes.fadeIn();
	  } else {
		filter = '.' + filter;
		boxes.filter(filter).fadeIn();
	  }
	    $('html, body').animate({
	      scrollTop: $("#first-scroll").offset().top
	    }, 1400);
		  return false;
		});
});


$(document).ready(function() {
	$('.move-to-select').on('click', function() {
		var moveString = $('.move-up').val();
		$('#moved-to').append($('<option>', {
		    value: 1,
		    text: moveString
		}));
	});
	$('.remove-option').on('click', function() {
		$("select#moved-to option:selected").remove();
	});
});










$(document).ready(function() {
    $('.checkbox.deductible-options input[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
            $('.hidden-options-01').fadeIn();
        }
        else if($(this).is(":not(:checked)")){
            $('.hidden-options-01').fadeOut();
        }
    });
});


$(document).ready(function() {
	$('.move-to-select-second').on('click', function() {
		var moveString = $('.move-up-second').val();
		$('#moved-to-second').append($('<option>', {
		    value: 1,
		    text: moveString
		}));
	});
	$('.hidden-options-01 .remove-option').on('click', function() {
		$("select#moved-to-second option:selected").remove();
	});
});

















$(document).ready(function() {
    $('.checkbox.limit-options input[type="checkbox"]').click(function(){
        if($(this).is(":checked")){
            $('.hidden-options-02').fadeIn();
        }
        else if($(this).is(":not(:checked)")){
            $('.hidden-options-02').fadeOut();
        }
    });
});


$(document).ready(function() {
	$('.move-to-select-third').on('click', function() {
		var moveString = $('.move-up-third').val();
		$('#moved-to-third').append($('<option>', {
		    value: 1,
		    text: moveString
		}));
	});
	$('.hidden-options-02 .remove-option').on('click', function() {
		$("select#moved-to-third option:selected").remove();
	});
});










$(window).scroll(function() {
    if ($(this).scrollTop() > 50 ) {
        $('.scrolltop:hidden').stop(true, true).fadeIn();
    } else {
        $('.scrolltop').stop(true, true).fadeOut();
    }
});
$(function(){$(".scroll").click(function(){$("html,body").animate({scrollTop:$(".thetop").offset().top},"1000");return false})})











